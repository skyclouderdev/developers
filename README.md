# README #

### Description 
This is a Template for the git Workshop, please follow the following steps

This workshop is to go through most general git commands and later will exercise the slides described in
[workshop presentation](https://docs.google.com/presentation/d/1b-fziX2jMvnppfyn7sNrWkVelpDRDaJfuffvasWKstA/edit#slide=id.p).

Please subscribe on [Git Source](https://www.udacity.com/course/how-to-use-git-and-github--ud775) to build your concepts on git.

Please click [here](https://www.atlassian.com/git/tutorials) for git Tutorials


### Steps
* create a folder on your machine by using the commmnad mkdir as in
```
#!
mkdir [folder name]
cd [to folder name]
``` 
* run the following command 
```
#!
 git init
``` 
 
* Copy the HTTPS url from right side of Overview section
```
#!
git remote add origin [past the url here] 
``` 
* Run the following commands to pull the code
```
#!
git fetch && git checkout [Needed Branch]
```


### Deployments

Please follow the following rules

* Create feature branch from master
* Work always on feature branch
* after the feature is ready merge it to the devtest
* deploy on devtest by using jenkins
* test and verify
* merge to QAtest after unit test on devtest
* deploy on QA by using jenkins
* After successful QA tests merge to master 
* Tag master with new version 
* use jenkins to deploy live application


Following are the link for available environments

1. Please click [here](http://falckkampagnen.dk/git_workshop/live/) to access the live environment.
1. Please click [here](http://falckkampagnen.dk/git_workshop/qa/) to access the QA environment.
1. Please click [here](http://falckkampagnen.dk/git_workshop/dev/) to access the devtest environment.